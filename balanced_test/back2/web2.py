from flask import request, Flask
import json
web2 = Flask(__name__)
@web2.route('/')
def hello_world():
    return 'Assalam alikum, this is Web2 :) '
if __name__ == '__main__':
    web2.run(debug=True, host='0.0.0.0')
