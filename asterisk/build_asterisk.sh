#!/usr/bin/env bash

useradd --system asterisk
apt-get update -qq
apt-get install --yes -qq --no-install-recommends --no-install-suggests \
  autoconf \
  binutils-dev \
  build-essential \
  ca-certificates \
  curl \
  file \
  git \
  libcurl4-openssl-dev \
  libedit-dev \
  libgsm1-dev \
  libogg-dev \
  libpopt-dev \
  libresample1-dev \
  libspandsp-dev \
  libspeex-dev \
  libspeexdsp-dev \
  libsqlite3-dev \
  libsrtp2-dev \
  libssl-dev \
  libvorbis-dev \
  libxml2-dev \
  libxslt1-dev \
  odbcinst \
  portaudio19-dev \
  procps \
  unixodbc \
  unixodbc-dev \
  uuid \
  uuid-dev \
  xmlstarlet \
> /dev/null

cd /usr/src/

( \
  git clone -b 18 http://gerrit.asterisk.org/asterisk asterisk \
) &>/dev/null
cd asterisk

./configure --with-resample \
            --with-pjproject-bundled \
            --with-jansson-bundled > /dev/null
make menuselect/menuselect menuselect-tree menuselect.makeopts
# disable BUILD_NATIVE to avoid platform issues
menuselect/menuselect --disable BUILD_NATIVE menuselect.makeopts

# enable good things
menuselect/menuselect --enable BETTER_BACKTRACES menuselect.makeopts


: ${JOBS:=$(( $(nproc) + $(nproc) / 2 ))}
make -j ${JOBS} all > /dev/null || make -j ${JOBS} all
make install > /dev/null
make samples > /dev/null

sed -i -E 's/^;(run)(user|group)/\1\2/' /etc/asterisk/asterisk.conf


mkdir -p /usr/src/codecs/opus
cd /usr/src/codecs/opus
curl -sL http://downloads.digium.com/pub/telephony/codec_opus/${OPUS_CODEC}.tar.gz | tar --strip-components 1 -xz
cp *.so /usr/lib/asterisk/modules/
cp codec_opus_config-en_US.xml /var/lib/asterisk/documentation/

mkdir -p /etc/asterisk/ \
         /var/spool/asterisk/fax

chown -R asterisk:asterisk /etc/asterisk \
                           /var/*/asterisk \
                           /usr/*/asterisk
chmod -R 750 /var/spool/asterisk

cd /
rm -rf /usr/src/asterisk \
       /usr/src/codecs
apt-get --yes -qq purge \
         autoconf \
         build-essential \
         bzip2 \
         cpp \
         m4 \
         make \
         patch \
         perl \
         perl-modules \
         pkg-config \
         xz-utils \
         ${DEVPKGS} \
       > /dev/null
